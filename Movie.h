//
// Created by Haley Manning on 2/3/17.
//

#ifndef DISNEYMOVIES_MOVIE_H
#define DISNEYMOVIES_MOVIE_H

#include <iostream>
#include <sstream>
#include <algorithm>
#include <string>

using namespace std;

class Movie {
private:
  int id;
  string title;
  int timesSeen;
  int year;
  int rating;

public:
  Movie();
  Movie(string);
  Movie(int, string, int);
  int getId(){ return id; }
  int getYear(){ return year; }
  string getTitle(){ return title; }
  int getTimesSeen(){ return timesSeen; }
  void markAsWatched(){ timesSeen++; }
  int getRating(){ return rating; }
  void rateMovie(int rate){ rating=rate; }
};


#endif //DISNEYMOVIES_MOVIE_H
