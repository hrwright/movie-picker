//
// Created by Haley Manning on 2/3/17.
//

#ifndef DISNEYMOVIES_MOVIEPICKER_H
#define DISNEYMOVIES_MOVIEPICKER_H

#include <iostream>
#include <vector>
#include <fstream>
#include "Movie.h"

using namespace std;

class MoviePicker {
private:
  unsigned long int randNum;
  vector<Movie> movies;
  string filename;
  int numSeen;
  string choice;

  void parseFile();
  void printMainMenu();
  void addMovie();
  void printMovies();
  void rateMovie();
  unsigned long int randNumGenerator();
  void randomMovie();
  void sortByName();
  void sortByDate();
  void sortByRating();
  void sortByYear();
  void sortByViews();
  void sort();
  void printSearchResults(string);
  int returnPos(string);
  void deleteMovie();
  void exportData();
public:
  MoviePicker();
  MoviePicker(string);
};


#endif //DISNEYMOVIES_MOVIEPICKER_H
