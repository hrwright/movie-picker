//
// Created by Haley Manning on 2/3/17.
//

#include "Movie.h"

//empty constructor
Movie::Movie() {}

//parses a line and creates a movie
Movie::Movie(string line) {
  stringstream ss;
  string i, s, y, r;

  ss.str(line);
  getline(ss, i, ',');
  getline(ss, title, ',');
  getline(ss, s, ',');
  getline(ss, y, ',');
  getline(ss, r, ',');

  id=atoi(i.c_str());
  timesSeen=atoi(s.c_str());
  year=atoi(y.c_str());
  rating=atoi(r.c_str());

  transform(title.begin(), title.end(), title.begin(), ::tolower);
}

Movie::Movie(int i, string t, int y) {
  id=i;
  title=t;
  year=y;
  rating=0;
  timesSeen=0;
}