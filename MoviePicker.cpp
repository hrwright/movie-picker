//
// Created by Haley Manning on 2/3/17.
//

#include "MoviePicker.h"

//empty constructor
MoviePicker::MoviePicker() {}

//constructor that does something
MoviePicker::MoviePicker(string input) {
  filename=input;
  string option;

  srand((unsigned int)time(NULL));
  parseFile();

  while(choice !="quit"){
    printMainMenu();
    if(choice == "random"){ randomMovie(); }
    else if(choice == "search"){
      string word;
      cout<<"What movie are you looking for?\n";
      cin.ignore();
      getline(cin, word);
      printSearchResults(word);
    }
    else if(choice == "add"){ addMovie(); }
    else if(choice == "rate"){ rateMovie(); }
    else if(choice == "print"){ printMovies(); }
    else if(choice == "delete"){ deleteMovie(); }
    else if(choice == "sort"){ sort(); }
    else if(choice == "quit"){ exportData(); }
    else if(choice != "quit"){
      cout<<"Not a valid option. Try again.\n-------------------------\n\n";
    }
  }
}

//parses the input file
void MoviePicker::parseFile(){
  string line,num;
  ifstream readFile;
  readFile.open(filename);
  readFile>>num;
  numSeen=atoi(num.c_str());
  getline(readFile,line);
  while(!readFile.eof()){
    getline(readFile, line);
    movies.push_back(Movie(line));
  }
  readFile.close();
  randNum=randNumGenerator();
}

//a random number generator
unsigned long int MoviePicker::randNumGenerator() {
  return randNum=rand()%movies.size()+1;
}

//prints the main menu
void MoviePicker::printMainMenu() {
  cout<<"Choose one of the following options: \n";
  cout<<"random: choose a random movie.\n";
  cout<<"search: search for a movie.\n";
  cout<<"add: add a new movie.\n";
  cout<<"rate: rate a disney movie.\n";
  cout<<"print: print all movies.\n";
  cout<<"delete: delete a movie.\n";
  cout<<"sort: sort your movies list.\n";
  cout<<"quit: quit the program.\n";
  cout<<"Option: ";
  cin>>choice;
  cout<<endl;
}

//chooses a random movie for you
void MoviePicker::randomMovie(){
  char answer='n';
  randNum=randNumGenerator();
  cout<<movies[randNum].getTitle()<<" ----- "<<movies[randNum].getYear()<<endl;
  cout<<"Would you like to watch this movie or quit the randomness(y/n/q)? ";
  cin>>answer;
  cout<<endl;
  while(answer=='n'){
    randNum=randNumGenerator();
    cout<<movies[randNum].getTitle()<<" ----- "<<movies[randNum].getYear()<<endl;
    if(movies[randNum].getTimesSeen() != 0) cout<<"You have seen this movie "<<movies[randNum].getTimesSeen()<<" times. With a rating of "<<movies[randNum].getRating()<<"\n";
    cout<<"Would you like to watch this movie or quit(y/n/q)? ";
    cin>>answer;
    cout<<endl;
  }
  if(answer=='y'){
    cout<<"Do you want to mark this movie as watched? (y/n) ";
    cin>>answer;
    cout<<endl;
    if(answer=='y'){
      movies[randNum].markAsWatched();
      numSeen++;
    }
  }
  if(answer=='q') choice="quit";
}

//adds a movie to the database
void MoviePicker::addMovie() {
  int id = movies.size()+1;
  string title;
  int year;
  cout<<"What is the title? ";
  cin.ignore();
  getline(cin, title);
  transform(title.begin(), title.end(), title.begin(), ::tolower);
  cout<<"What is the year it first showed? ";
  cin>>year;
  movies.push_back(Movie(id, title, year));
  cout<<"Your movie has been added.\n\n";
}

//prints all of the movies to the screen
void MoviePicker::printMovies(){
  for(int i=0; i<movies.size(); i++){
    cout<<movies[i].getTitle()<<" ---Year: "<<movies[i].getYear()<<" ---Rating: "<<movies[i].getRating()<<" stars"<<" ---Views: "<<movies[i].getTimesSeen()<<endl;
  }
  cout<<endl;
}

//can rate a movie
void MoviePicker::rateMovie(){
  string title;
  char seen, confirm;
  int rate;
  string foundMovie;
  int index;
  cout<<"What movie would you like to rate? ";
  cin.ignore();
  getline(cin, title);
  transform(title.begin(), title.end(), title.begin(), ::tolower);
  index=returnPos(title);
  if(index == -1){ cout<<"Cannot find that movie. Be more specific."; }
  else{
    foundMovie=movies[index].getTitle();
    cout<<"Do you want to rate "<<foundMovie<<"?(y/n) "<<endl;
    cin>>confirm;
    if(confirm=='y'){
      cout<<"Have you seen this movie?(y/n) ";
      cin>>seen;
      if(seen=='y'){
        cout<<"rate your movie from 1-5: ";
        numSeen++;
        movies[index].markAsWatched();
        cin>>rate;
        while(rate <1 || rate >5 ){
          cout<<"Try again: ";
          cin>>rate;
        }
        movies[index].rateMovie(rate);
        cout<<endl;
      }
      else if(seen=='n'){
        cout<<"You must see the movie before you can rate it.\n";
      }
    }
    else if(confirm=='n'){
      cout<<"You'll need to be more specific with your movie title.\n";
    }
  }
}

//sort by name
void MoviePicker::sortByName() {
  int i, j, flag = 1;
  Movie temp;
  int numLength = movies.size();
  for(i = 1; (i <= numLength) && flag; i++) {
    flag = 0;
    for (j=0; j < (numLength -1); j++) {
      if (movies[j+1].getTitle() < movies[j].getTitle()) {
        temp = movies[j];
        movies[j] = movies[j+1];
        movies[j+1] = temp;
        flag = 1;
      }
    }
  }
}

//sort by date
void MoviePicker::sortByDate() {
  int i, j, flag = 1;
  Movie temp;
  int numLength = movies.size();
  for(i = 1; (i <= numLength) && flag; i++) {
    flag = 0;
    for (j=0; j < (numLength -1); j++) {
      if (movies[j+1].getId() < movies[j].getId()) {
        temp = movies[j];
        movies[j] = movies[j+1];
        movies[j+1] = temp;
        flag = 1;
      }
    }
  }
}

//sort by rating
void MoviePicker::sortByRating() {
  int i, j, flag = 1;
  Movie temp;
  int numLength = movies.size();
  for(i = 1; (i <= numLength) && flag; i++) {
    flag = 0;
    for (j=0; j < (numLength -1); j++) {
      if (movies[j+1].getRating() > movies[j].getRating()) {
        temp = movies[j];
        movies[j] = movies[j+1];
        movies[j+1] = temp;
        flag = 1;
      }
    }
  }
}

//sort by year movie came out
void MoviePicker::sortByYear(){
  int i, j, flag = 1;
  Movie temp;
  int numLength = movies.size();
  for(i = 1; (i <= numLength) && flag; i++) {
    flag = 0;
    for (j=0; j < (numLength -1); j++) {
      if (movies[j+1].getYear() < movies[j].getYear()) {
        temp = movies[j];
        movies[j] = movies[j+1];
        movies[j+1] = temp;
        flag = 1;
      }
    }
  }
}

//sort by views
void MoviePicker::sortByViews() {
  int i, j, flag = 1;
  Movie temp;
  int numLength = movies.size();
  for(i = 1; (i <= numLength) && flag; i++) {
    flag = 0;
    for (j=0; j < (numLength -1); j++) {
      if (movies[j+1].getTimesSeen() < movies[j].getTimesSeen()) {
        temp = movies[j];
        movies[j] = movies[j+1];
        movies[j+1] = temp;
        flag = 1;
      }
    }
  }
}

//sort and pick your sort
void MoviePicker::sort() {
  string option;
  cout<<"Would you like to sort by name (name), date added (date), rating (rate), year (year), or times seen (view)? ";
  cin>>option;
  if(option == "name"){
    sortByName();
  }
  else if(option == "date"){
    sortByDate();
  }
  else if(option == "rate"){
    sortByRating();
  }
  else if(option == "view"){
    sortByViews();
  }
  else if(option == "year"){
    sortByYear();
  }
  printMovies();
}

//search for a word
void MoviePicker::printSearchResults(string word){
  size_t found;

  transform(word.begin(), word.end(), word.begin(), ::tolower);
  for(int i=0; i<movies.size(); i++){
    found=movies[i].getTitle().find(word);
    if (found != std::string::npos){
      cout<<movies[i].getTitle()<<endl;
    }
  }
  cout<<endl;
}

//delete movies from the database
void MoviePicker::deleteMovie(){
  string name;
  char response;
  int index;
  cout<<"What is the name of the movie you're wanting to delete.";
  cin.ignore();
  getline(cin, name);
  index=returnPos(name);
  if(index == -1){
    cout<<"Sorry, we couldn't find that movie.\n";
  } else {
    cout<<"Are you sure you want to delete "<<movies[index].getTitle()<<"? (y/n) ";
    cin>>response;
  }
  cout<<"Deleting "<<movies[index].getTitle()<<endl;
  movies.erase(movies.begin()+(index));
}

//finds the position of the searched thing
int MoviePicker::returnPos(string word) {
  size_t found;
  transform(word.begin(), word.end(), word.begin(), ::tolower);
  for(int i=0; i<movies.size(); i++){
    found=movies[i].getTitle().find(word);
    if (found != std::string::npos){ return i; }
  }
  return -1;
}

//exports/saves the data when the user quits
void MoviePicker::exportData() {
  ofstream output;
  output.open(filename);
  output<<numSeen<<"\n";
  for(int i=0; i<movies.size(); i++){
    Movie m = movies[i];
    output<<m.getId()<<",";
    output<<m.getTitle()<<",";
    output<<m.getTimesSeen()<<",";
    output<<m.getYear()<<",";
    output<<m.getRating()<<"\n";
  }
}